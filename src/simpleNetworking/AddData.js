import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {TOKEN, BASE_URL} from './url';

const AddData = ({navigation, route, show, onClose}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  var item = route.params;

  useEffect(() => {
    if (route.params) {
      const data = route.params.item;
      console.log(route.params);
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  const postData = async () => {
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      alert('Data Mobil berhasil ditambahkan');
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const editData = async () => {
    const body = [
      {
        _uuid: item._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      alert('Data Mobil berhasil dirubah');
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <Modal visible={show} transparent={true}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.4)',
        }}>
        <View
          style={{
            width: 300,
            height: 450,
            backgroundColor: '#fff',
            justifyContent: 'center',
            borderRadius: 5,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: '10%',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 10,
                marginLeft: 10,
                marginRight: 15,
              }}
              onPress={onClose}>
              <Icon name="times" size={20} color="#000" />
            </TouchableOpacity>
            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
              {item ? 'Edit Data' : 'Tambah Data'}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              padding: 15,
            }}>
            <View>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Nama Mobil
              </Text>
              <TextInput
                placeholder="Masukkan Nama Mobil"
                style={styles.txtInput}
                onChangeText={text => setNamaMobil(text)}
                value={namaMobil}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Total Kilometer
              </Text>
              <TextInput
                placeholder="contoh: 100 KM"
                style={styles.txtInput}
                onChangeText={text => setTotalKM(text)}
                value={totalKM}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Harga Mobil
              </Text>
              <TextInput
                placeholder="Masukkan Harga Mobil"
                style={styles.txtInput}
                keyboardType="number-pad"
                onChangeText={text => setHargaMobil(text)}
                value={hargaMobil}
              />
            </View>
            <TouchableOpacity
              style={styles.btnAdd}
              onPress={() => (item ? editData() : postData())}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                {item ? 'Edit Data' : 'Tambah Data'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
