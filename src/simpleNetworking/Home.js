import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Modal,
  ActivityIndicator,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useIsFocused} from '@react-navigation/native';
import AddData from './AddData';

export default function Home({navigation}) {
  const isFocused = useIsFocused();
  const [dataMobil, setDataMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selected, setSelected] = useState(null);
  // const [showAddData, setShowAddData] = useState(false);
  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const getDataMobil = async () => {
    try {
      setIsLoading(true);
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
      });
      const result = await response.json();
      console.log('Success: ', result);
      setIsLoading(false);
      setDataMobil(result.items);
    } catch (error) {
      console.error('Error :', error);
    }
  };

  const convertCurrency = (nominal, currency) => {
    if (typeof nominal !== 'undefined' && nominal !== null) {
      let rupiah = '';
      const nominalref = nominal.toString().split('').reverse().join('');
      for (let i = 0; i < nominalref.length; i++) {
        if (i % 3 === 0) {
          rupiah += nominalref.substr(i, 3) + '.';
        }
      }

      if (currency) {
        return (
          currency +
          rupiah
            .split('', rupiah.length - 1)
            .reverse()
            .join('')
        );
      } else {
        return rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('');
      }
    } else {
      return '';
    }
  };

  const deleteData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      setShowModal(false);
      navigation.replace('Home');
      alert('Data Mobil berhasil dihapus');
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      {/* <AddData show={showAddData} onClose={() => setShowAddData(false)} /> */}
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}
            onPress={() => {
              setSelected(item);
              setShowModal(true);
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{uri: item.unitImage}}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
      <Modal visible={isLoading}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" />
        </View>
      </Modal>
      <Modal visible={showModal} transparent={true}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.4)',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: 250,
              height: 100,
              padding: 10,
              borderRadius: 10,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 5,
            }}>
            <TouchableOpacity
              onPress={() => {
                setShowModal(false);
              }}>
              <Icon name="times" color="black" size={20} />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
              }}>
              <TouchableOpacity
                style={{
                  backgroundColor: 'green',
                  borderRadius: 10,
                  paddingVertical: 5,
                  alignItems: 'center',
                  width: '45%',
                }}
                onPress={() => {
                  setShowModal(false);
                  navigation.navigate('AddData', {
                    dataMobil: dataMobil,
                    item: selected,
                  });
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>Edit</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: 'red',
                  borderRadius: 10,
                  paddingVertical: 5,
                  alignItems: 'center',
                  width: '45%',
                }}
                onPress={() => deleteData()}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>Delete</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({});
